FROM node:14-slim

WORKDIR /app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY ./backend/package.json /app/
RUN npm install

COPY ./backend/ /app/

COPY ./frontend/ /build
RUN cd /build && CYPRESS_INSTALL_BINARY=0 yarn && yarn build && mv dist /frontend

ENV PORT 3001
EXPOSE $PORT
CMD [ "npm", "start" ]
