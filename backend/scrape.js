const axios = require('axios').default
const cheerio = require('cheerio')
const { Colleague, sequelize } = require('./db')
const fs = require('fs')

const ninjaRegex = /([^\ud000-\udfff]+)\s*(\W+)\s*(\w+)/ // Split string on emoji letters

const scrape = async () => {
  const content = await axios.get('https://tretton37.com/meet')
  const $ = cheerio.load(content.data)
  $('.ninjas .ninja-summary').each((index, child) => {
    const text = $(child).find('h1 a').text()
    const img = $(child).find('img').attr('src')
    const filename = img.substring(img.lastIndexOf('/') + 1)
    const result = ninjaRegex.exec(text)
    const ninja = {
      name: result[1],
      country: result[2],
      city: result[3],
      image: filename
    }
    Colleague.upsert(ninja) // Update or insert
    const saveImageTo = __dirname + '/images/' + filename
    axios.get(img, {responseType: 'stream'}).then(res => {
      const writeStream = fs.createWriteStream(saveImageTo)
      res.data.pipe(writeStream)
      writeStream.on('finish', () => {
        console.log('Image written to ' + saveImageTo)
      })
      writeStream.on('error', (error) => {
        throw error
      })
    })
  })
}

const run = () => {
  sequelize
    .authenticate()
    .then(scrape)
    .catch((error) => {
      console.warn('DB not ready, retrying scraping in 5s...')
      setTimeout(() => {
        run()
      }, 5000)
    })
}

run()
