const Sequelize = require('sequelize').Sequelize

const db = process.env.DB || 'tretton37'
const user = process.env.DB_USER || 'tretton37'
const password = process.env.DB_PASSWORD || 'tretton37'

const sequelize = new Sequelize(db, user, password, {
  host: process.env.DB_HOST || 'mariadb',
  dialect: 'mariadb',
  dialectOptions: {
    charset: 'utf8mb4' // utf8mb4 necessary for emojis
  },
  define: {
    charset: 'utf8mb4',
    collate: 'utf8mb4_general_ci'
  }
})

const Colleague = sequelize.define('colleague', {
  name: {
    // @ts-ignore
    type: Sequelize.STRING,
  },
  image: {
    // @ts-ignore
    type: Sequelize.STRING,
    unique: true
  },

  city: {
    // @ts-ignore
    type: Sequelize.STRING,
  },
  country: {
    // @ts-ignore
    type: Sequelize.STRING,
  },
})

// Sync DB structure to model(s) on first connect
const sync = () => {
  sequelize
    .authenticate()
    .then(() => {
      Colleague.sync({ alter: true })
        // .then(() => {
        //   Colleague.create({
        //     name: 'Jonas A',
        //     image: 'jonas.png',
        //   })
        // })
    })
    .catch(() => {
      console.warn('DB not ready, retrying in 5s...')
      setTimeout(() => {
        sync()
      }, 5000)
    })
}
sync()

module.exports = {
  Colleague,
  sequelize
}
