const express = require('express')
const fs = require('fs')
const cors = require('cors')
const { Colleague } = require('./db')

const server = express()

server.use(cors())

server.get('/api/colleagues', async (req, res) => {
  const colleagues = await Colleague.findAll()
  res.json(colleagues)
})

server.get('/images/:filename', (req, res) => {
  res.sendFile(__dirname + '/images/' + req.params.filename)
})

// Serve frontend if /frontend exists (when running in production) else redirect to development frontend
if (fs.existsSync('/frontend/index.html')) {
  server.use('/', express.static('/frontend'))

  server.use('*', function (req, res) {
    res.sendFile('/frontend/index.html')
  })
} else {
  server.use('*', function (req, res) {
    res.redirect('http://localhost:3000')
  })
}

server.listen(3001)

console.log('Backend open on port 3001')
