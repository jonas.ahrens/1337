import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './assets/tailwind.css'

Vue.config.productionTip = false

Vue.prototype.$axios = axios
Vue.prototype.$backend = process.env.VUE_APP_API || 'https://tretton37.onlydevs.net/'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
