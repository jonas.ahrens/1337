import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const backend = process.env.VUE_APP_API || 'https://tretton37.onlydevs.net/'

export default new Vuex.Store({
  state: {
    colleagues: []
  },
  mutations: {
    setColleagues (state, payload) {
      state.colleagues = payload
    }
  },
  actions: {
    async fetchColleagues ({ commit }) {
      const colleagues = await axios.get(backend + 'api/colleagues')
      if (colleagues && colleagues.data) {
        commit('setColleagues', colleagues.data)
      }
    }
  },
  modules: {
  }
})
