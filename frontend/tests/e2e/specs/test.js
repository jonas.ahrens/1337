// https://docs.cypress.io/api/introduction/api.html

describe('tretton37 e2e test', () => {
  it('Renders the app root url', () => {
    cy.visit('/')
    cy.contains('h3', 'The fellowship of the tretton37')
  })

  it('Populates colleagues', () => {
    cy.get('.colleague-container').find('img.profile').its('length').should('be.gte', 100)
  })

  it('Sorts by office', () => {
    cy.get('select#sort').select('Office')
    cy.wait(1500)
    cy.get('.colleague').first().find('.colleague-office').invoke('text').should('match', /Borl/)
  })

  it('Filters by free text', () => {
    cy.get('input#filter').type('nys')
    cy.wait(1500)
    cy.get('.colleague-container').find('img.profile').its('length').should('be.lte', 20)
  })

})
