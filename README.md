# Tretton37 Assignment

## Running & Project Design

This project contains a Docker Compose configuration which includes everything needed for running it locally. To run it simply run `docker-compose up`. It uses ports 3000, 3001 and 3002 for frontend, backend and phpMyAdmin (database administration), respectively. It is also available on <https://tretton37.onlydevs.net/>.  

The project consists of a [Vue frontend](frontend/src) with Tailwind CSS. It fetches its data from a [Node.js backend](backend/) using data scraped from the real tretton37 site (<https://tretton37.com/meet>).

## Stories Chosen

Most of the effort in this iteration of the project focuses on the backend/scraping and CI/CD parts of the assignment as well as orchestration of containers to make the application work as a whole. The stories chosen reflect this focus.   

### Design/Accessibility

#### Responsive design (2 pt)

### Functionality

#### Sort by name and office (1 pt)

#### Filter by name and office (1 pt)

#### Available on a public url (1 pt)

The pipeline automatically deploys to <https://tretton37.onlydevs.net/>.

#### CI/CD pipeline (1 pt)

The [pipeline](https://gitlab.com/jonas.ahrens/1337/-/pipelines) is defined in [.gitlab-ci.yml](.gitlab-ci.yml). It runs Cypress for testing and uses GitLab Auto DevOps images for building and deploying to a Kubernetes cluster on Linode. Building is done with the [Dockerfile](Dockerfile) and pushes to GitLab's integrated container image repository. The deploy stage uses a [Helm chart](chart/) based on the [Bitnami Node Chart](https://github.com/bitnami/charts/tree/master/bitnami/node). 

#### Build your own API by scraping the current site (4 pt)

The API is made with a simple Express backend with Sequelize and MariaDB to store scraped data. The [scraping](backend/scrape.js) script is set to run as a separate container with a shared volume for storing images. In production it runs on a [cronjob](chart/templates/cronjob.yaml) Kubernetes resource every 24 hours to update the data.

### Testing/QA

#### Works in Chrome, Firefox, Edge (1 pt)

#### End-to-end testing (2 pt)

Some testing is partially implemented (using Cypress) in the CI/CD pipeline before proceeding to the build stage.

